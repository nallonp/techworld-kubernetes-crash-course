kubectl apply -f mongo-config.yaml
kubectl apply -f mongo-secret.yaml
kubectl apply -f mongo.yaml
kubectl apply -f mongo-express.yaml
kubectl apply -f webapp.yaml
kubectl apply -f admin-user.yaml
kubectl apply -f role-binding.yaml
kubectl apply -f dashboard-ingress.yaml